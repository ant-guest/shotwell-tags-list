#
#  This gawk script will scan through a dump looking
# for file names of videos and pictures and the 
# associated tags using a dump from the sqlite3 
# shotwell photo.db file using SQLite3 command:
#
#  $ echo .dump | sqlite3 photo.db > photo_dump.sql
#
# then:
#
#  $ gawk -E TagTableDump.gawk < photo_dump.sql
#
#
# I am not sure I will ever need this again so I may
# never fix it for new versions, but at least it works
# for what I need it to do:
#
# "VersionTable VALUES(1,20,'0.15.0',NULL);"
# (taken from the top few lines of the above dump)
#
# File names are expected to be sane (no control 
# characters) and no tabs "\t".  Tag names are 
# expected to be alphanumeric with " ", "_" or "-"
# and also no tabs "\t".  The tab character is used
# as a separator between tag name and file name if 
# you expect to do any post processing on output 
# like sorting...
#
# Any errors are reported while scanning or at the end,
# including unused tags.
#
# If you find your shotwell tags look very messed up it
# is much easier to restore from a known good backup, but
# also clear the .cache for shotwell.
#
# Note: This is not guaranteed to be a complete list as 
# any untagged pictures or videos will not be listed at
# the end.  i.e. this is a tag dump script as file names
# can be easily gotten from using a find command on the
# file system...
#
# Other Note: I have no idea what the limits are of gawk
# as far as number of array elements or size of overall
# things...
#
# Output is: 
#
#   console messages    : errors found while processing
#
#   and up to six files
#     - f01_unk             : strange things
#     - f02_unused_tags     : tags that were not used
#     - f03_vid_tags        : video file tags
#     - f03_vid_tags_sorted : video file tags sorted
#     - f04_pic_tags        : picture file tags
#     - f04_pic_tags_sorted : picture file tags sorted
#
#

BEGIN {
      }

/^INSERT INTO PhotoTable VALUES/ {
      if (match($0,"'([[:print:]])*'") != 0) {
        filename = substr($0,index($0,"'")+1,RLENGTH-2)
        flength = index(filename,"'")
        filename = substr(filename,1,flength-1)
        fileno = substr($0,31)
        fileno = strtonum(fileno)
        PicList[fileno] = filename
#       printf "%s %s\n", fileno, filename
        }
      else if(match($0,"'([[:cntrl:]])*'") != 0) {
        printf "Error: picture # %d filename contains control character(s).\n", substr($0,31)
        }
      else {
        printf "Error: unusual picture input %s\n", $0
        }
      }

/^INSERT INTO VideoTable VALUES/ {
      if (match($0,"'([[:print:]])*'") != 0) {
        filename = substr($0,index($0,"'")+1,RLENGTH-2)
        flength = index(filename,"'")
        filename = substr(filename,1,flength-1)
        fileno = substr($0,31)
        fileno = strtonum(fileno)
        VidList[fileno] = filename
#       printf "%s %s\n", fileno, filename
        }
      else if(match($0,"'([[:cntrl:]])*'") != 0) {
        printf "Error: video # %d filename contains control character(s).\n", substr($0,31)
        }
      else {
        printf "Error: unusual video input %s\n", $0
        }
      }

/^INSERT INTO TagTable VALUES/ {
      if (match ($0,"'([[:alnum:]]|_|-| )*'") != 0) {
        tagname = substr($0,index($0,"'")+1,RLENGTH-2)
#       printf "%s\n", tagname
        split($0,parts,"'")
        if (parts[4] != "") {
          thumbs = parts[4]
          split(thumbs,parts,",")
          for (key in parts) {
            if (parts[key] != "") {
#             print key, parts[key]
              if (substr(parts[key],1,5) == "thumb") {
                picno = strtonum("0x" substr(parts[key],6,length(parts[key])))
#               printf " %s %s\n", tagname, picno
                PicTagTable[tagname,picno] = picno
                }
              else if (substr(parts[key],1,6) == "video-") {
                vidno = strtonum("0x" substr(parts[key],7,length(parts[key])))
#               printf " %s %s\n", tagname, vidno
                VidTagTable[tagname,vidno] = vidno
                }
              else {
                printf "Error: we don't know what parts[key] %s is.\n" parts[key]
                }
              }
            }
          }
        else {
          notusedtagtable[tagname] = tagname
          }
        }
      else if (tagname != "") {
        unktagtable[tagname] = tagname
        }
      }

END {

  # we don't know what order the dump was made so we cannot
  # generate the final lists until the entire dump has been 
  # scanned... 

    f01_unk = "f01_unk"
    printf "\n\nUnknown Tags:\n"
    for (tagind in unktagtable) {
      printf "  %s\n", unktagtable[tagind]
      printf "  %s\n", unktagtable[tagind] > f01_unk
      }
    print "\n"

    f02_unused_tags = "f02_unused_tags"
    printf "Unused Tags:\n" 
    for (tagind in notusedtagtable) {
      printf "  %s\n", notusedtagtable[tagind]
      printf "  %s\n", notusedtagtable[tagind] > f02_unused_tags
      }
    print "\n"

    f03_vid_tags = "f03_vid_tags"
#   printf "Video Tags:\n"
    for (tagind in VidTagTable) {
#     printf "  %s %s\n", tagind, VidTagTable[tagind]
      num = split(tagind,parts,"\034")
      if (num == 2) {
        tname = parts[1]
        fname = VidList[parts[2]]
        if ((parts[2] in VidList) && (length(fname) > 0)) {
          printf "%s\t%s\n", tname, fname > f03_vid_tags
          }
        }
      else {
        printf "Error: missing parts in VidTagTable index %s\n", tagind
        }
      }
#   print "\n"

    f04_pic_tags = "f04_pic_tags"
#   printf "Picture Tags:\n"
    for (tagind in PicTagTable) {
#     printf "  %s %s\n", tagind, PicTagTable[tagind]
      num = split(tagind,parts,"\034")
      if (num == 2) {
        tname = parts[1]
        fname = PicList[parts[2]]
        if ((parts[2] in PicList) && (length(fname) > 0)) {
          printf "%s\t%s\n", tname, fname > f04_pic_tags
          }
        }
      else {
        printf "Error: missing parts in PicTagTable index %s\n", tagind
        }
      }
#   print "\n" 

#   for (vidind in VidList) {
#     printf "  %s\n", VidList[vidind]
#     }
#   print "\n" 
#   for (picind in PicList) {
#     printf "  %s\n", PicList[picind]
#     }
#   print "\n" 
    }
